#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  1 13:35:43 2018
@author: sage
"""

import re
from common_functions import basic_check
from items import link_keys
import isbnlib

class RawRef():
    '''Class used to extract a year from a raw li Wikipedia element'''
    
    def __init__(self, raw=None):
        '''raw refers to the raw html of the page'''
        
        self.raw = raw
        
        if self.raw != None:
            self.txt = self.raw.get_text()
        else:
            self.txt = None
            
        self.rm_keys = ['..mw-parser', 'retrieved', 'archived ', 'isbn', 'accessed', 'doi:']
        self.year = -1
        
        self.website = False
        
    def remove_blue(self):
        '''Remove link/ titles of things (aka stuff in blue)'''
        
        blue_things = self.raw.find_all('a')
        blue_stuff = []
        
        for thing in blue_things:
            text = self.remove_non_ascii(thing.get_text())
            
            if text != 'Archived':
                blue_stuff.append(text)
        
        for stuff in blue_stuff:
            self.txt = self.txt.replace(stuff,'')
            
    def remove_non_ascii(self, txt):
        '''Converts all non ascii weird characters to - '''

        txt = ''.join([i if ord(i) < 128 else '-' for i in txt])
        return txt
        
    def general_remove(self, keyword):
        '''This function removes everything after the keyword as well.'''
        
        R = self.txt.find(keyword)

        if R != -1:
            self.txt = self.txt[:R]
            
    def remove_pages(self):
        '''Given a chunk of text., removes common page tags and the following numbers,
           which if not removed could easily be confused as valid years'''
        
        #Pad each side of the text, just in case
        self.txt = ' ' + self.txt + ' '
        
        #Check for pages following APA style citation nonsense, as well as other ways of
        #indicating page number - more sensible ways perhaps
        re_keys = [r'\d+\(\d+\)', r'\d+\ \(\d{1,3}\)', r'\ p\.', r'\ pp\.', r'\(pt\ 1\)\:', r'p\d+', r'\d+th',
                   r'\(page', r'\ pages', r'Vol\.', r'\(p\.', r'\d+\:\d+', r'[a-z]+\d+']
        
        related_nums = [2, 2, 2, 2, 4, 1, 1, 2, 2, 2, 2, 2, 1, 1]
        
        for i in range(len(re_keys)):
            
            r = re_keys[i]
            spots = re.findall(r, self.txt)
            
            for spot in spots:
                loc = self.txt.find(spot)
                
                pg = ' '.join(self.txt[loc:].split()[:related_nums[i]])
                self.txt = self.txt.replace(pg, '')
            
            
    def remove_titles(self):
        'Removes the titles from the text - sometimes the title contains a year like number'''
        
        titles = re.findall(r'\"(.*?)\"', self.txt)

        for t in titles:
            self.txt = self.txt.replace(t, '')
            
    def remove_links(self):
        '''Removes common links from the text, e.g. in the case where ther url has a number'''

        txt_split = self.txt.split()

        for t in txt_split:
            for k in link_keys:
                if k in t:
                    self.txt = self.txt.replace(t, '')
                    self.website = True
                    
                    
    def find_digit(self, nums, key):
        '''Finds the first digit of length key from the array nums'''
        
        if self.year == -1:
            
            for n in nums:
                if len(n) == key and self.year == -1:
                    
                    if basic_check(n):
                        self.year = n

    def find_3_or_4_digits(self, nums, two=False):
        '''Wrapper function to search for 4-3 digit and potentially 2 digit numbers within the array nums'''
    
        self.find_digit(nums, 4)
        self.find_digit(nums, 3)
        
        if two:
            self.find_digit(nums, 2)
                    
    def check_for(self, k, two=False):
        '''Generalized function to check for 3 cases, searching for a year within ()'s, []'s and
           if k = None, then within the full chunk of text itself '''
        
        if self.year == -1:
    
            sub = ''

            if k == '()':
                sub = ' '.join(re.findall(r'\((.*?)\)', self.txt))

            elif k == '[]':
                sub = ' '.join(re.findall(r'\[(.*?)\]', self.txt))

            else:
                sub = self.txt
                two = False # False always too many exceptions
                
            #merge commas
            sub = sub.replace(',','')

            nums = re.findall(r'\d+', sub)
            
            #Consider the numbers sorted by highest first
            nums = sorted([int(n) for n in nums], reverse=True)
            nums = [str(n) for n in nums]
            
            if self.website:
                two = False

            if len(nums) != 0:
                self.find_3_or_4_digits(nums, two=two)
            
    def check_isolated(self):
        '''Checks to make sure the year found is not isolated, for whatever reason it seems that
           all the years found in refrences should not have a space on either side'''
        
        if self.year != -1:
            if ' ' + self.year + ' ' in self.txt:
                self.year = -1
                
    def check_for_BC(self):
        '''Checks the text around where the year was found to see if this year should be bc.
           If bc, set the year to be negative'''
        
        if self.year != -1:
            indx = self.txt.find(self.year)
            
            if indx != -1:

                start = indx
                end = indx
    
                if indx  - 4 >= 0:
                    start = indx - 4
                else:
                    start = 0
    
                if end + 8 < len(self.txt):
                    end = len(self.txt) - 1
    
                if ' bce' in self.txt[start:end] or ' bc' in self.txt[start:end]:
                    self.year = '-' + self.year
                    
    def check_ISBN(self):
        
        if self.year == -1:
            
            #Since ISBN was removed earlier, lets look for it in the raw text
            txt = self.raw.get_text()
            
            #Use the built in function to search for a valid ISBN
            isbn = isbnlib.get_isbnlike(txt, level='normal')
    
            if len(isbn) > 0:
                
                #If found, returned as list, just get the first entry- there shouldn't be more then one anyway
                isbn = isbn[0]
                
                #If found... see if an entry exists
                try:
                    self.year = isbnlib.meta(isbn)['Year']
                except:
                    pass
                
    def cite_ref_check(self):
        '''Checks all of the raw hrefs present, and determines if this ref is just a link to something in the bibliography,
           if so scrape the year from the name. Also checks to see if the reference contains a link to a webpage'''
        
        for a in self.raw.find_all('a', href=True):
        
            href = a['href']
            
            if '#CITEREF' in href:
            
                nums = re.findall(r"\d+", href)
                
                #This list of nums should be reveresed as year tends to be cose to the end
                nums.reverse()
                
                self.find_3_or_4_digits(nums)
                
            for key in link_keys:
                if key in href:
                    self.website = True
                
            
    def rerun(self):
        '''If nothing found on the first run, repeat the scrape with tweaked more lax paramaters'''
        
        if self.year == -1:
            
            #Reset text, and re-run
            self.txt = self.raw.get_text()
            self.extract_year(title=True)

    def extract_year(self, title=False):
        '''Main function designed to extract the year of the refrence'''
        
        #Checks the raw reference first
        self.cite_ref_check()
        
        #Replace weird/non ascii chars with '-'
        self.txt = self.remove_non_ascii(self.txt)
        
        #Try to replace 'blue' stuff
        if not title:
            self.remove_blue()
        
        #Make lowercase
        self.txt = self.txt.lower()
        
        #Clean retrieved, isbn, title, pages
        for key in self.rm_keys:
            self.general_remove(key)
        
        #Remove pages, titles, links
        self.remove_pages()
        self.remove_links()
        
        if not title:
            self.remove_titles()

        #Check for dates in []'s then parentheses, then all
        self.check_for('[]', False)
        self.check_for('()', False)
        
        if not self.website:
            self.check_for(None)
        
        #Check for two digits
        self.check_for('[]', True)
        self.check_for('()', True)
        
        #Year don't appear isolated, e.g. space on either side
        self.check_isolated()

        #Check if it is a bce year
        self.check_for_BC()
        
        #Check to see if there is a valid isbn that we can extract the year for
        self.check_ISBN()
        
        #After first run do this
        if not title:
            self.rerun()