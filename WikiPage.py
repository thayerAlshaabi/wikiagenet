#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  1 13:36:55 2018
@author: sage
"""

from bs4 import BeautifulSoup
from RawRef import RawRef
from items import person_keys, not_person_keys, placement_keys, common_person_keys, common_non_person_keys
from common_functions import chunk_to_lowest, get_links
from Person import Person

class WikiPage():
    ''' Main WikiPage object, designed to be associated with one specific page or topic, though the
        class wrapper can easily be re-called and loaded with different pages as needed'''

    def __init__(self, loader, name=None):
        '''Init function, where loader is a WikiLoader object and name refers to the title of a Wiki page.
           If name given, load html '''

        self.placement_keys = placement_keys

        self.name = name
        self.html = None

        self.loader = loader #WikiLoader object

        self.items = [[] for key in self.placement_keys]
        self.item_years = [[] for key in self.placement_keys]

        self.person_keys = person_keys
        self.not_person_keys = not_person_keys
        self.people = {}


        if name != None:
            self.init_page(name)
            if self.page != None:
                self.get_html()
        else:
            self.page = None
        
        #Load the set of people who have not been seen into NP
        self.NP = set()
        self.load_not_people()

    def init_page(self, name):
        '''Sets the Wikipage object to a name'''

        self.name = name
        self.page = self.loader.get_page(name)

    def get_html(self):
        '''Call wikipedia pages get html'''

        self.html = self.page.html()
        
    def scrape_all(self):
        '''Wrapper method to scrape all fields'''
        
        self.mine()
        self.extract_years(test_mode=False)
        
        self.extract_links()
        self.check_people()
        
        self.clear()
        #self.conv_people() #Optional~
        
    def clear(self):
        '''Method designed to be called after all scraping has been concluded, to clear unused fields'''
        
        if self.html:
            del self.html
            
        if self.page:
            del self.page
            
        if len(self.people) > 0:
            for person in self.people:
                self.people[person].clear()
                
        if self.NP:
            del self.NP
            
        if self.loader:
            del self.loader

    def first_pass_person(self, link):
        '''Function to run as a first pass for determining if the name of a link is a person or not'''

        link = link.lower()
        
        #First check is just, has this entry been seen before, are they known to be not person
        if link in self.NP:
            return False

        #Common key words which indicate the page is not a person
        keys = common_non_person_keys

        #Append also the name of the page
        keys.append(self.name.lower())

        for key in keys:
            if key in link:
                return False

        return True

    def get_people_links(self):
        '''Runs first_person_pass over the links extracted by the original Wikipedia object'''

        links = list(set(self.links))
        links = [l for l in links if l in links if self.first_pass_person(l)]
        return links

    def is_person(self, page):
        '''Checks if a given page is a person or not, based on the title and elements present within
           a persons pages'''

        #List of common keys which could appear in titles
        p_keys = common_person_keys

        title = page.title.lower()
        html = page.html()

        for key in p_keys:
            if key in title:
                return True

        html = str(html).lower()
        
        for key in self.not_person_keys:
            if key in html:
                return False

        for key in self.person_keys:
            if key in html:
                return True

        return False

    def check_people(self):
        '''Main function designed to collect the people present on this Wiki page,
           creating a Person object for each link identified to be a person'''

        if self.page != None:
            links = self.get_people_links()
            pages = self.loader.get_pages(links, ret_name=True)

            for p_obj in pages:

                page = p_obj[0]
                link = p_obj[1]
                
                if page != None:

                    if self.is_person(page):
                        self.people[page.title] = Person(page, scrape=True)
                    
                    #If not a person, make sure to add to the list of not people
                    else:    
                        self.add_not_people(link.lower())

    def get_outline(self):
        '''Create an outline of the categories present'''

        self.outline = {}

        for key in self.placement_keys:
            self.outline[key] = self.html.find(key)

        self.outline_sorted = sorted(self.outline.values())

    def get_html_crop(self, k):
        '''Given a key, find a relevant crop of the html containing at best just that category.'''

        spot = self.outline_sorted.index(self.outline[self.placement_keys[k]])
        start = self.outline_sorted[spot]

        try:
            end = self.outline_sorted[spot+1]
        except:
            end = -1

        return start, end

    def get_items(self, cropped_html):
        '''Extract items from wiki html - works on notes, refs and simmilar
           Depreciated Function~~~'''

        soup = BeautifulSoup(cropped_html, 'lxml')

        #Notes are listed as
        if cropped_html.find('<ol class="references">') != -1:
            r = soup.find("ol", {"class": "references"})

        else:
            try:
                r = soup.findAll("div", {"class": "refbegin"})[0]
            except:
                return None

        items = [li for li in r.findAll('li')]

        return items
    
    def alt_get_items(self, cropped_html):
        '''Currently in use method for extracting raw ref items from any given section'''
        
        soup = BeautifulSoup(cropped_html, 'lxml')
        items = [li for li in soup.findAll('li')]
        
        return items
        

    def mine(self):
        '''Get the raw li element / items for all relevant present categories.'''

        if self.page != None:
            self.get_outline()

             #Indx w/ support right now
             
            for i in range(len(self.placement_keys)):
                
                if self.outline[self.placement_keys[i]] != -1:
                    
                    start, end = self.get_html_crop(i)
                
                    if i in [1,2,3,6]:
                        self.items[i] = self.alt_get_items(self.html[start:end]) #For these entries, get_items was previously used
                        
                    elif i in [4,5,7,8]:
                        self.items[i] = self.alt_get_items(self.html[start:end])
                

    def extract_years(self, test_mode=False):
        '''Run extract years on all items, if test mode is true, print out the text and extracted year'''

        #Indx w/ support right now
        for i in range(len(self.placement_keys)):

            if self.items[i]:
                self.item_years[i] = []

                if test_mode:
                    print(self.placement_keys[i])

                for item in self.items[i]:

                    raw = RawRef(item)

                    if test_mode:
                        print(raw.txt)
                        print('------------')

                    raw.extract_year()

                    if raw.year != -1:
                        self.item_years[i].append(raw.year)

                    if test_mode:
                        print(raw.year)#, '***', raw.txt)
                        print()
                        #print('^^^^^^^^^^^^^^^^^^^')

    def extract_links(self):
        '''From the html of the page, extract the raw form of all wiki links present'''
        
        
        try:
            cropped_html = chunk_to_lowest(self.html)
            
            #Filter out the vert navbox links
            ind = cropped_html.find('class="vertical-navbox')
            
            while ind != -1:
                end = cropped_html[ind:].find('</table>')
                
                if end != -1:
                    cropped_html = cropped_html[:ind] + cropped_html[ind+end:]
                    ind = cropped_html.find('class="vertical-navbox')
                    
                else:
                    ind = -1
                    print('Maybe an error in removing a navbox')
            
            ss = BeautifulSoup(cropped_html, 'lxml')
            self.links = get_links(ss)
        except:
            self.links = None
            
            
    def load_not_people(self):
        '''Loads the list of confirmed not people links into self.NP'''
        
        try:
            with open('NP.txt', 'r') as f:
                lines = f.readlines()
                
                for line in lines:
                    self.NP.add(line.strip())
        
        except FileNotFoundError:
            with open('NP.txt', 'w') as f:
                pass
            
    def add_not_people(self, link):
        '''Adds a new entry to the not people list/set'''
        
        #Somewhat needless additional check- don't want repeats
        if link not in self.NP:
    
            with open('NP.txt', 'a') as f:
                f.write(link)
                f.write('\n')
                
            self.NP.add(link)  #Keep it updated for this round aswell, again just in case
            
            
    def conv_people(self):
        '''Convert self.people form a dict of person objects, to a dict with just info'''
        
        people = {}
        
        if self.people:
            for p in self.people:
                people[p] = (self.people[p].born, self.people[p].dead, self.people[p].all_years)
                
        del self.people
        
        self.people = people
                
            
        
