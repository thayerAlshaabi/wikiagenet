#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 14:55:13 2018

@author: sage
"""
import numpy as np
import networkx as nx
from collections import Counter
import plotly.graph_objs as go

def make_branch_to_person_graph(branches):
    ''' Helper function to, given a list of branches compute a graph'''
    
    G = nx.DiGraph()  

    for b in branches:

        if b not in G:
            G.add_node(b)

        people = branches[b].get_people()

        for p in people:
            if p not in G:
                G.add_node(p)

            G.add_edge(b,p)

    return G


def get_formatted_preds(G):
    ''' Helper function format branch predictions nicely for use with plotly'''

    people_dict = {}

    for n in G:
        predecessors = list(G.predecessors(n))

        if len(predecessors) > 0:

            for x in range(0, len(predecessors), 5):
                predecessors[x] = '<br>' + predecessors[x]

            people_dict[n] = 'Member of:' + ', '.join(predecessors)
    
    return people_dict


def format_year(e):
    '''Sub method used to easily format year to string'''
    
    if e == -1:
        return ' '
    return str(e)

def round_func(x, base):
    '''Custom round a given x to base, base.'''

    return round(base * round(float(x)/base),2)

def get_people_heights(means, base):
    '''Computes the height a person should be at, when plotted'''
    
    rounded = [round_func(m, base) for m in means]
    val_dict = Counter(rounded)

    y_vals = []
    entry_count = {}

    for entry in rounded:

        try:
            entry_count[entry] += 1
        except KeyError:
            entry_count[entry] = 1

        freq = val_dict[entry]
        interval = 1 / (freq+1)

        y_vals.append(entry_count[entry] * interval)

    return y_vals

def get_shared_links(branches, people=False):
    
    G_links = nx.Graph()
    cons = []
    
    b_links = {}
    for b in branches:
        page = branches[b].page
        
        if people:
            pl = set(page.people.keys())
            
        else:
            pl = page.links
            
        b_links[b] = pl
        
        
    for b1 in b_links:
        for b2 in b_links:
            if b1 != b2:
                connection = len(b_links[b1] & b_links[b2])
                if connection > 0:
               
                    cons.append(connection)
                    G_links.add_edge(b1, b2, weight = connection)
                    
    return G_links, cons, b_links
                    
def get_weights(cons):
    
    weight_dict = {}

    i = 0
    for val in np.unique(cons):
        weight_dict[val] = i
        i+=1
    
    return weight_dict


def make_network_figure(branches, people, edge_scale, node_scale):


    G_links, cons, b_links = get_shared_links(branches, people=people)
    weight_dict = get_weights(cons)
    
    mx_node = max([len(b_links[node]) for node in G_links.nodes()])
    mx_edge = max([weight_dict[c] for c in np.unique(cons)])
    
    linenorm = mx_edge / edge_scale
    nodenorm = mx_node / node_scale
    
    edge_lists = []
    pos = nx.spring_layout(G_links, weight='weight', k=6)
    
    for key in weight_dict:
        
        edge_trace = go.Scatter(
        x=[],
        y=[],
        line=dict(width=key/linenorm,color='#888'),
        hoverinfo = None,
        mode='lines')
        
        edge_lists.append(edge_trace)
        
        
    for e in G_links.edges():
        
        edge_data = G_links.get_edge_data(e[0], e[1])
        w = edge_data['weight']
        
        x0, y0 = pos[e[0]]
        x1, y1 = pos[e[1]]
        
        i = weight_dict[w]
        
        edge_lists[i]['x'] += tuple([x0, x1, None])
        edge_lists[i]['y'] += tuple([y0, y1, None])
        
        
    node_sizes = []
    
    for node in G_links.nodes():
        node_sizes.append((len(b_links[node]) / nodenorm) + (nodenorm))
        
        
    node_trace = go.Scatter(
        x=[],
        y=[],
        text=[],
        mode='markers',
        hoverinfo='text',
        marker=dict(
            showscale=True,
            colorscale='YlGnBu',
            reversescale=True,
            color=[],
            size=node_sizes,
            colorbar=dict(
                thickness=15,
                title='Node Connections',
                xanchor='left',
                titleside='right'
            ),
            line=dict(width=2)))
    
    for node in G_links.nodes():
        x, y = pos[node]
        node_trace['x'] += tuple([x])
        node_trace['y'] += tuple([y])
        
        
    for node in G_links.nodes():
        node_trace['text']+=tuple([node.replace('_',' ')])
        
    data = edge_lists+[node_trace]
    layout = go.Layout(
                title='<br> Branch Relationships',
                titlefont=dict(size=16),
                showlegend=False,
                hovermode='closest',
                margin=dict(b=20,l=5,r=5,t=40),
                annotations=[ dict(
                    text="None",
                    showarrow=False,
                    xref="paper", yref="paper",
                    x=0.005, y=-0.002 ) ],
                xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
                yaxis=dict(showgrid=False, zeroline=False, showticklabels=False))
    
    return data, layout
   


