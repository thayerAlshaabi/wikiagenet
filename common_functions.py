#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  9 11:20:19 2018

@author: sage
"""

from items import placement_keys, raw_placement_keys
from bs4 import BeautifulSoup
import re, urllib

def chunk_to_lowest(content, html=True):
    '''Different pages might not have some of these categories, so it searches for all of them,
           and cuts off the content at the earlierst one of these to appear'''

    if html:
        keys = placement_keys
    else:
        keys = raw_placement_keys

    locs = [content.find(key) for key in keys]
    locs = [l for l in locs if l != -1]

    if len(locs) == 0:
        return -1

    return content[:min(locs)]


def basic_check(n):
    '''Basic sanity check for numbers found, checking to make sure they don't
       start with 0, and are reasonable, where n is the number'''

    if int(n) > 2020 or n.startswith('0'):
        return False
    return True


def get_links(html_soup):
    '''From the html of the page, extract the raw form of all wiki links present'''

    links = set()
    for link in html_soup.findAll('a', attrs={'href': re.compile("^/wiki")}):

        l = link.get('href')

        if ':' not in l:
            #Remove the weird reference within a page
            ind = l.find('#')
            if ind != -1:
                l = l[:ind]
                
            l = l.replace('/wiki/','')
            links.add(urllib.parse.unquote(l))

    return links

def get_first_link(html_chunk):
    '''Given a chunk of html, return the first wiki link found'''
    
    link = None
    
    soup = BeautifulSoup(html_chunk, 'lxml')
    x = soup.find('a', attrs={'href': re.compile("^/wiki")})
    try:
        l = x.get('href')
        
        if ':' not in l:
            ind = l.find('#')
            if ind != -1:
                l = l[:ind]
                
            link = l.replace('/wiki/','')
            link = urllib.parse.unquote(link)
    except:
        pass
    
    return link


def get_years(wikipage, section_keys=None, verbose=0):
    ''' Return a dict of years found in each section of a wikipage '''

    years = {}
    if section_keys != None: # list years found in a given section
        for key in section_keys:
            k = 'id=\"'+key+'\"'
            if k in placement_keys:
                idx = placement_keys.index(k)
                years[key] = wikipage.item_years[idx]

                if verbose:
                    print('\n--- {} // [{}] years found!'.format(
                        k, len(wikipage.item_years[idx]))
                    )
                    print(wikipage.item_years[idx])
            else:
                print(k, 'not found in placement_keys!')


    else: # list all years
        for i in range(len(wikipage.items)):
            key = placement_keys[i].strip('id=\"')
            years[key] = wikipage.item_years[i]

            if verbose:
                print('[{n}]\tyears --- {src}'.format(
                    n=len(wikipage.item_years[i]), src=key)
                )
    return years
