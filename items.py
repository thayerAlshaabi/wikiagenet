#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  1 13:42:41 2018
@author: sage

Helper file which simply contains useful (maybe) static lists
"""

#Main keys present on a wikipedia page's HTML, associated with that header item. These are useful for locating if categories
#are present on a page and where they are specifically
placement_keys = ['id="See_also"', 'id="Notes"', 'id="Footnotes"', 'id="References"',
                  'id="Further_reading"', 'id="Bibliography"', 'id="Notes_and_references"',
                   'id="Sources">Sources', 'id="Primary_sources"', 'id="External_links"', 'id="Related">Related',
                   'class="printfooter"']

raw_placement_keys = ["See also", "Notes", "Footnotes", "References",
                  "Further reading", "Bibliography", "Notes and references",
                  "External links"]

#Keys used to determine if a wikipedia href contains a url to a website, vs. an internal link or something else ~
link_keys = ['.com', '.org', '.net', '.edu', '.gov']

#The following are common key words used to distinguish if a page is a person or not based on just
#the title of the link. Used as just a quick first pass.
common_person_keys = ['(author)', '(writer)', '(mathematician)', '(chemist)',
                      '(physicist)']

common_non_person_keys = ['history', 'glossary', 'lists', 'list of', 'outline',
                'philosophy of', 'applied', 'fundamental', 'theory',
                'university', 'chemical', 'definition', 'website', 'field',
                'education in', 'nature', 'life', 'climate', 'cell']

#List of html keys associated with an HTML page being a person.
person_keys = ['id="Origin_and_family"', 'id="Life_and_career', 'id="Biography"', 'id="Early_Life"',
              '<th scope="row">born</th>', '<th scope="row">died</th>', 'id="Important_works"',
              'id="Death"', 'class="infobox biography vcard"',
              'id="Career"', 'id="Positions_and_awards"', 'id="Ancestry"',
              'id="Early_Life_&amp;_Career"', 'id="Life_and_work"',
              'id="Education_and_career"', 'id="Academic_career"']
person_keys = [key.lower() for key in person_keys]

not_person_keys = ['id="Definitions"', 'id="Medical_uses"', 'id="Risks"', 'id="Geography"', 'id="Economy"',
                   'id="Purpose"', 'id="Measurement"', 'id="Causes"', 'id="Classification"', 'id="Epidemiology"',
                   'id="Ecosystems"']
not_person_keys = [key.lower() for key in not_person_keys]

#Example lists of known pages which are people, and respectively not people.
people = ['Antoine Lavoisier', 'Willard Gibbs', 'Thorstein Veblen', 'John T. Struble',
          'Isaac S. Struble',
         'William McKinley', 'Abū al-Rayhān al-Bīrūnī', 'Al-Kindi', 'Ahmad Y Hassan',
          'Alan T. Hutton',
         'Antoine Lavoisier', 'Carl Wilhelm Scheele', 'Henry Cavendish', 'Henry Moseley',
          'Humphry Davy', 'Plato', 'Glaucon', 'John Burnet (classicist)', 'Ann Henderson (campaigner)',
         'Frances H. Arnold', 'Francis Bacon', 'Galileo', 'Hippocrates',
          'Georges Lemaître', 'Henrietta Leavitt', 'Albert Einstein', 'Al-Kindi', 'Pliny the Younger',
         'Alcibiades', 'Barack Obama']

not_people = ['Chemistry', 'Anglicanism', 'History of thermodynamics', 'Technology',
              'Iowa State University',
             'Church of Scotland', 'Johnson County, Iowa', 'Amount of substance',
              'Analytical chemistry',
              'Ancient Egypt',
             'Arab world', 'Arrhenius equation', 'Argon', 'Atomic nucleus',
              'Caffeine', 'Chemical equation', 'Base (chemistry)',
             'Biophysical chemistry', 'Carbon dioxide', 'Byzantine', 'Ilm (Arabic)',
             'Eclecticism', 'Knowledge', 'Formal logic', 'Academic journal', 'Advocacy group',
             'Analytical mechanics', 'Aircraft', 'Annals of Science']

def get_outline_keys(t):
    '''Return topic (t = name of topic) specific outline keys. Used for extracting branches from a topic'''
    
    #These keys are used to locate the start of where branches or topics are listed
    section_keys = ['branches_of', 'areas_of', 'subjects',
                    'fields_of', 'forms_of', 'fields_of_study_of',
                     'subfields']
    
    section_keys = ['id="' + key for key in section_keys]
    
    #Stop keys are used to locate any possible ending location where branches/topics stop getting listed
    stop_keys = ['history_of', 'history', 'nature_of',  'general_'+t+'_concepts', 'theories',
                'methods_and_frameworks', t+'_organizations', t+'_scholars', t+'_methods',
                'related_fields', t+'_by_region', 'types_of', 'related_disciplines',
                'general_', ' species']
    
    stop_keys = ['id="' + key for key in stop_keys]

    
    return section_keys, stop_keys