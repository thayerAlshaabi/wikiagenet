#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  1 14:45:28 2018
@author: sage
"""

import re
from common_functions import chunk_to_lowest, basic_check
from bs4 import BeautifulSoup
import numpy as np

class Person():
    '''Person class designed to make scraping a wikipedia persons years associated with them easier'''
    
    def __init__(self, page, scrape=True):
        '''Name refers to the name of the person, and content refers to the chunk of text.
           By default try to scrape everything'''
        
        self.page = page
        self.name = page.title
        self.content = page.content
        self.life = -1
        self.all_years = []
    
        if scrape:
            self.scrape_all()
        
    def scrape_all(self):
        '''Main wrapper function to extract the year a person was born/died, and if
           not None then grab all of the years within the article'''
        
        self.extract_life()
        self.secondary_extract_life()
        
        self.merge()
        
        if self.born != -1:
            self.grab_all()
            
        if len(self.all_years) == 0:
            self.all_years = np.array([])
            
    def clear(self):
        '''Function designed to be called after scrape_all to remove now unneeded info'''
        
        if self.page:
            del self.page
        if self.content:
            del self.content
            
    def merge(self):
        ''' A persons born and death date are extracted in two ways, this method is designed
            to merge the results from the two seperate scraping procedures  - it should therefore be
            called only after '''
        
        #If there exists a self.life entry, convert to born died
        if self.life[0] != -1:
            
            born = self.life[0]
            dead = -1
            
            if len(self.life) == 1:
                
                #If a person only has one date associated with them, and it is before 1900, assume they died 50 years later
                if born < 1900:
                    dead = born + 50
                else:
                    dead = -1
            else:
                dead = self.life[1]
                
        else:
            born = -1
            dead = -1
                
                
        #Special case - just have death date
        if self.born == -1 and self.dead != -1:
            self.born = self.dead - 50
                
        #Now compare
        if self.born != born:
            
            #Keep self.born unless, it is -1
            if self.born == -1:
                self.born = born
        
        if self.dead != dead:
            
            if self.dead == -1:
                self.dead = dead
                
        self.born = int(self.born)
        self.dead = int(self.dead)
        
    def find_digit(self, year, nums, key):
        '''Explicity finds the first digit of length key from the array, nums.
           Where year is just a placeholder to see if a valid year as been found already'''
            
        if year == -1:
            
            for n in nums:
                if len(n) == key and year == -1:
                    if basic_check(n):
                        year = n
        
        return year

    def find_x_digits(self, nums, x=4):
        '''From an array of numbers searches recursively for nums of length 4 down to x'''
    
        year = -1
        key = 4
        
        while year == -1 and key >= x:
            year = self.find_digit(year, nums, key)
            key -= 1
            
        return year

    def BC_check(self, chunk):
        '''Checks a chunk of text to see if it should be BC'''
        
        if ' BC' in chunk or ' BCE' in chunk:
            return True
        return False

    def replacer(self, chunk):
        '''Somewhat sketchy method ~ but replaces all instances of xth century with the year x,
           not perfect obviously, since should be less then x, but atleast captures something'''
        
        #Rough replacement
        r_dict = {'1st century': '100', '2nd century': '200', '3rd century' : '300'}
        
        for x in range(4,20):
            key = str(x) + 'th century'
            r_dict[key] = str(x*100)
            
        for key in r_dict:
            chunk = chunk.replace(key, r_dict[key])
            
        return chunk

    def get_born_dead(self, nums):
        '''Given an array of numbers, determine the year the person was born and if they died and return'''
        
        born = self.find_x_digits(nums, 2)
        dead = -1
        
        if born != -1:
            nums.remove(born)
            
            #-7 to 7 being a reasonable range for two years to be apart
            for x in range(-7,7):
                try:
                    nums.remove(str(int(born)+x))
                except:
                    pass
    
            dead = self.find_x_digits(nums, len(born)-1)
            
        if dead != -1:
            return [int(born), int(dead)]
        else:
            return [int(born)]
        
        
    def get_next(self, txt):
        '''Sub-routine for get_chunk, to make finding paren's easier'''
        
        o = txt.find('(')
        c = txt.find(')')
        
        if o == -1 and c == -1:
            return None, None
        if o == -1:
            return ')', c
        if c == -1:
            return '(', o
        if o > c:
            return ')', c
        else:
            return '(', o

    def get_chunk(self, txt):
        '''Sub-routine for get_chunks, getting one singular chunk'''
        
        cnt = 1
        s = 0
        
        while(cnt > 0):
            
            n, spot = self.get_next(txt[s:])
            
            if n == None:
                return txt[:s]
            
            s += spot+1
            
            if n == ')':
                cnt -= 1
            else:
                cnt += 1
        
        return txt[:s-1]
        
    
    def get_chunks(self, txt):
        '''Function designed to capture all content within (), taking into account closing order, ect...
           Returns the first item as everything within the first (, then everything within the second that appears, ect...'''
        
        chunks = []
        s = txt.find('(')
        
        while s != -1:
    
            chunks.append(self.get_chunk(txt[s+1:]))
            
            spt = txt[s+1:].find('(')
            
            if spt == -1:
                s = -1
            else:
                s += spt+1
     
        return chunks
    
    def extract_life(self):
        '''Main function to extract the year a person was born and died, if they did '''
        
        #The text based extraction method is limited to the first 500 characters
        cont = self.content[:500]
        
        #Sophisticated (haha) way of grabing content within ()'s as dates for people ussually are
        ps = self.get_chunks(cont)
        ps = [self.replacer(p) for p in ps]
        
        #Init flags + Variables
        bc = False
        nums = []
        x = 0
        
        #Search through until valid num found
        while len(nums) == 0 and x < len(ps):
            nums = re.findall(r'\d+', ps[x])
            bc = self.BC_check(ps[x])
            
            x+=1
        
        #If no numbers found within parens, search first 200 characters only
        if len(nums) == 0:
            cont = self.replacer(cont[:200])
            nums = re.findall(r'\d+', cont)
            bc = self.BC_check(cont)
        
        #Helper function to turn array of numbers into explicit born and/or died
        life = self.get_born_dead(nums)
        
        if bc:
            life = [-l for l in life]
        
        #Make sure life is in the right order (in the case of BC)
        life.sort()
        
        self.life = life
        
        
    def quick_find(self, text, key):
        '''Helper function to find potential years within a subset of text as indicated by a key used for indexing'''
    
        year = -1
        ind = text.lower().find(key)
    
        if ind != -1:
            nums = re.findall(r'\d+', text[ind:])
            year = self.find_x_digits(nums, x=2)
            
        return year
        
    def secondary_extract_life(self):
        '''Robust method for finding the year a person was born and/or died -
           though requires the presence of a v-box, therefore this is presented as a secondary method
           for finding this information'''
        
        html = self.page.html()
        soup = BeautifulSoup(html, 'lxml')
        
        born = -1
        dead = -1
        
        bc = False
        
        bday = soup.find(class_="bday")

        if bday != None:
            bday_nums = re.findall(r'\d+', bday.text)
            born = self.find_x_digits(bday_nums, x=2)
            
            if born != -1:
                bc = self.BC_check(bday.text)
            
        for element in soup.find_all(class_="vcard"):
            text = element.text
            
            if born == -1:
                born = self.quick_find(text, 'born')
                
                if born != -1:
                    bc = self.BC_check(text)
                
            if dead == -1:
                dead = self.quick_find(text, 'died')
                
                if dead != -1:
                    bc = self.BC_check(text)
                    
        if bc and born != -1:
            born = '-' + born
        if bc and dead != -1:
            dead = '-' + dead
        
        #Set the two class wide variables accordingly
        self.born = int(born)
        self.dead = int(dead)

    def grab_all(self):
        '''Function designed to 'grab all' of the years present on a page between when 
           a person was born, and when they died (if they did). Stores years found in all_years'''
        
        self.content = chunk_to_lowest(self.content, html=False)
        
        #Used for selection of years
        born = self.born
        dead = self.dead
        
        if dead == -1:
            dead = 2019
        
        #Gets all numbers
        try:
            nums = re.findall(r'\d+', self.content)
        except:
            nums = []
            
        if len(nums) > 0:
        
            #Big (kind of) assumption here, that if the person is born on BC, all of the years mentioned between the two dates 
            #are also BC - assumes that it won't catch too many years actually mentioned in AD
            if born < 0:
                nums = [-int(n) for n in nums]
            else:
                nums = [int(n) for n in nums]
            
            self.all_years = [n for n in nums if n > born and n <= dead]
            self.all_years = np.array(self.all_years)
            
            