from WikiPage import WikiPage
from bs4 import BeautifulSoup
import common_functions as utils
from items import get_outline_keys
from Branch import Branch
import re

class Topic():

    def __init__(self,  # Constructor
        loader,         # an instance of WikiLoader class
        name,           # name of the branch (has to be a valid wiki page)
    ):

        self.page = WikiPage(loader, name)
        
        self.name = name
        self.loader = loader
        
        self.branch_links = set()

        if self.page != None:
            # scan a wikipage for any of the supported sections
            # defined in placement_keys, and stores then in self.page.items            
            self.page.scrape_all()

            self.branches = {}
            self.populate_branches()
            
        self.clear()
            
            
    def clear(self):
        
        if self.loader:
            del self.loader
                    
    def populate_branches(self):
        ''' Method to scan and then populate the branches for a given topic '''
        
        try:
            self.scan_outline_page()
        except:
            print('error w/ scanning outline page')
        
        #try:
        #    self.scan_outline_table()
        #except:
        #    print('error w/ scanning outline table')

        regex = re.compile(r'\S+_of_')
        rel_links = [i for i in self.branch_links if not regex.search(i) and
                     i != self.name.lower()]

        for link in rel_links:
            
            print('Adding a new branch [ {} ]...'.format(link))
            self.branches[link] = Branch(self.loader, link)
            
    def scan_outline_table(self):
        '''Scans outline table for branch links, adds to self.branch_links'''

        section_keys = [
            'Branches_of_'+self.name.lower(),
            'Areas_of_'+self.name.lower()
        ]

        for k in section_keys:
            idx = self.page.html.find('div id="'+k+'"')

            if idx != -1:
                soup = BeautifulSoup(self.page.html, 'lxml')

                r = soup.findAll('div', {'aria-labelledby': k})[0]
                
                items = utils.get_links(r)
                
                for item in items:
                    self.branch_links.add(item.lower())
                    
    def scan_outline_page(self):
        '''Scans outline of page fro branch links, adds to self.branch_links'''
        
        #Some of the stop keys are name specific- so should be accessed via this function
        section_keys, stop_keys = get_outline_keys(self.name)
        
        outline_name = 'outline_of_' + self.name
        outline_page = WikiPage(self.loader, outline_name)
        
        html = outline_page.html.lower()
        
        sec_locs = [html.find(key) for key in section_keys if html.find(key) != -1]
        stop_locs = [html.find(key) for key in stop_keys if html.find(key) != -1]
        
        start = min(sec_locs)
        end = min([s for s in stop_locs if s > start])
        chunk = html[start:end]

        for line in chunk.split('\n'):
            link = utils.get_first_link(line)
            
            if link and link != self.name:
                self.branch_links.add(link)
                
    def get_all_years(self, section_keys=None, verbose=0):
        ''' Get a dict of all years for the given topic '''

        listed_years = utils.get_years(self.page, section_keys, verbose)

        for b in self.branches.values():
            y = b.get_years(section_keys, verbose)

            for k,v in y.items():
                listed_years[k].extend(v)

        return listed_years


    def get_all_people(self):
        ''' Get a dict of all people for the given topic '''

        listed_people = self.page.people

        for b in self.branches.values():
            listed_people.update(b.page.people)

        return listed_people
