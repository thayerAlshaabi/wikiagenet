#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  1 13:52:30 2018
@author: sage
"""

import pickle, wikipedia, os
from multiprocessing import Pool

class WikiLoader():
    '''The WikiLoader class is designed to be called once in the start of a session, as it manages the
       calling/saving of Wikipedia page objects'''

    def __init__(self, dr='WikiPages/', save_mode=True, multi=0, verbose=0):
        ''' Init the object, where dr is the directory of locally saved Wiki pages, and
            save_mode refers to if the user would like to continue to save locally this usage'''

        self.dr = dr
        self.multi = multi

        if not os.path.exists(self.dr):
            os.mkdir(self.dr)

        self.get_existing()
        self.save_mode = save_mode
        self.verbose = verbose
        
        self.ret_name = False

    def get_existing(self):
        '''Searches the save directory for which pages already exist locally'''

        self.existing = os.listdir(self.dr)
        self.existing = set([f.replace('.wiki','') for f in self.existing])

    def save_new(self, name):
        '''Function to attempt to save a new article locally'''

        try:
            page = wikipedia.page(name, preload=True)
            page.html()

            with open(self.dr + name + '.wiki', 'wb') as f:
                pickle.dump(page, f)

            self.existing.add(name)

        except:
            
            if self.verbose > 0:
                print('error getting ', name)
            return None

    def return_new(self, name):
        '''This function is used when save mode is off to return an article directly'''

        try:
            page = wikipedia.page(name)
            page.html()

            return page

        except:
            
            if self.verbose > 0:
                print('error getting ', name)
            return None


    def load_page(self, name):
        ''' Loads an existing wiki object from local storage and returns it'''

        with open(self.dr + name + '.wiki', 'rb') as f:
            page = pickle.load(f)

        return page

    def get_page(self, name):
        '''Main function to be called on new queries, returns/saves/loads a page and returns it'''

        if self.save_mode:

            if name not in self.existing:
                self.save_new(name)

            #Page should now be loaded
            try:
                page = self.load_page(name)
            except:
                page = None

        #If save mode false
        else:

            if name in self.existing:
                try:
                    page = self.load_page(name)
                except:
                    page = None
            else:
                page = self.return_new(name)
                
        #If no page is found, sometimes searching for version w/o _ works instead
        if page == None and '_' in name:
            name = name.replace('_', ' ')
            page = self.get_page(name)
            
            if self.ret_name:
                page = page[0]
        
        if self.ret_name == True:
            return (page, name)
        else:
            return page

    def get_pages(self, names, ret_name=False):
        '''Given a list of names/links to scrape, goes through with multi_processing if specified and 
           gets the page ,then returns a list of pages, or if specified a list of (page, link_name) tuples.'''

        pages = []
        
        self.ret_name = ret_name

        if self.multi == 0:
            for name in names:
                pages.append(self.get_page(name))

        elif type(self.multi) == int:

            pool = Pool(processes=self.multi)
            pages = pool.map(self.get_page, names)
            pool.close()
            
        #By default reset ret_name to false
        self.ret_name = False
            
        return pages
