from WikiPage import WikiPage
import common_functions as utils

class Branch():

    def __init__(self,  # Constructor
        loader,         # an instance of WikiLoader class
        name,           # name of the branch (has to be a valid wiki page)
    ):

        self.page = WikiPage(loader, name)

        if self.page != None:
            # scan a wikipage for any of the supported sections
            # defined in placement_keys, and stores then in self.page.items
            
            self.page.scrape_all()
            
    def get_years(self, section_keys=None, verbose=0):
        ''' Return a dict of years found in each section of a wikipage '''

        return utils.get_years(self.page, section_keys, verbose)


    def get_people(self):
        ''' Return a dict of people found in the given wikipage '''

        return self.page.people
