import plotly.graph_objs as go
import plotly.figure_factory as ff
from ipywidgets import interactive, HBox, VBox

from dash import Dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from utilfuncs import load_topic, df_to_table, get_people_df
from plot_helpers import make_network_figure, scatter_plot

cats = ['Physics', 'Mathematics']
current_topic = load_topic(cats[0])
current_df = get_people_df(current_topic)

distplot = ff.create_distplot([current_df.year_mean], [cats[0]])
scatter_data, scatter_layout = scatter_plot(current_df)
branches_data, branches_layout = make_network_figure(current_topic.branches, 2, 30)

layout = [
    html.Div([
            html.Div( 
                dcc.Dropdown(
                    id="topic_dropdown",
                    options= [{"label": opt, "value": opt} for opt in cats],
                    value=cats[0],
                    clearable=False,
                    searchable=True
                ),
                className="two columns",
            ),
        ],
        className="row",
        style={"marginBottom": "10"},
    ),

    html.Div([
            html.Div([
                    html.P("Figure"),
                    dcc.Graph(
                        id='hist',
                        figure={
                            'data': distplot.data,
                            'layout': distplot.layout,
                        },
                        config=dict(displayModeBar=True),
                        style={"height": "90%", "width": "98%"},
                    ),
                ],
                className="columns chart_div",
            ),
        ],
        className="row",
        style={"marginBottom": "10"},
    ),

    html.Div([
            html.Div([
                    html.P("Figure"),
                    dcc.Graph(
                        id='people',
                        figure={
                            'data': scatter_data,
                            'layout': scatter_layout,
                        },
                        config=dict(displayModeBar=True),
                        style={"height": "90%", "width": "98%"},
                    ),
                ],
                className="columns chart_div",
            ),
        ], 
        className="row",
        style={"marginBottom": "10"},
    ),

    html.Div([
            html.Div([
                    html.P("Figure"),
                    dcc.Graph(
                        id='branches',
                        figure={
                            'data': branches_data,
                            'layout': branches_layout,
                        },
                        config=dict(displayModeBar=True),
                        style={"height": "90%", "width": "98%"},
                    ),
                ],
                className="columns chart_div",
            ),
        ], 
        className="row",
        style={
            "maxHeight": "500px",
            "marginBottom": "10"
        },
    ),

    html.Div([
            html.Div(
                df_to_table(current_df[['name', 'born', 'dead', 'desc']]),
                id="people_table",
                className="row",
                style={
                    "maxHeight": "350px",
                    "overflowY": "scroll",
                    "padding": "8",
                    "marginTop": "5",
                    "backgroundColor":"white",
                    "border": "1px solid #C8D4E3",
                    "borderRadius": "3px"
                },
            ),
        ],
        className="row",
        style={"marginTop": "5px"},
    ),
]