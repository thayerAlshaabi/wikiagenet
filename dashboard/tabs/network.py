import plotly.graph_objs as go
import plotly.figure_factory as ff
from ipywidgets import interactive, HBox, VBox

from dash import Dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from utilfuncs import load_topic, df_to_table, get_people_df

cats = ['Physics', 'Mathematics']
topics = [load_topic(c) for c in cats]
dfs = [get_people_df(t) for t in topics]

data = []
for i in range(len(dfs)):
    data.append(go.Histogram(
        x=dfs[i].year_mean,
        name=cats[i],
        histnorm='percent'
    ))
    
layout = [
    html.Div([
            html.Div([
                    html.P("Figure"),
                    dcc.Graph(
                        id='hist',
                        figure={
                            'data': data,
                            'layout': go.Layout(barmode='stack'),
                        }, 
                        config=dict(displayModeBar=True),
                        style={"height": "90%", "width": "98%"},
                    ), 
                ],
                className="columns chart_div",
            ),
        ],
        className="row",
        style={"marginTop": "5px"},
    ),
]