
import os.path, sys
import multiprocessing
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

import flask
import pandas as pd
from dash import Dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from tabs import home, network, topics


server = flask.Flask(__name__)
app = Dash(__name__, server=server)
app.config.suppress_callback_exceptions = True

app.layout = html.Div([

    html.Div([ # header
        html.Span("Wikinet Dashboard", className='app-title'),
        html.Div(
            html.Img(src='https://upload.wikimedia.org/wikipedia/en/7/70/Wikipedia-logo-crnagora.png',height="100%")
            ,style={"float":"right","height":"100%"})
        ],
        className="row header"
    ),

    # tabs
    html.Div([
        dcc.Tabs(
            id="tabs",
            style={"height":"20","verticalAlign":"middle"},
            children=[
                dcc.Tab(label="Home", value="home"),
                dcc.Tab(label="Network", value="network"),
                dcc.Tab(label="Topics", value="topics"),
                
            ],
            value="tabs",
        )],
        className="row tabs_div"
    ),

    # styles
    html.Div(id="tab_content", className="row", style={"margin": "2% 3%"}),
    html.Link(href="https://use.fontawesome.com/releases/v5.2.0/css/all.css",rel="stylesheet"),
    html.Link(href="https://cdn.rawgit.com/plotly/dash-app-stylesheets/2d266c578d2a6e8850ebce48fdb52759b2aef506/stylesheet-oil-and-gas.css",rel="stylesheet"),
    html.Link(href="https://fonts.googleapis.com/css?family=Dosis", rel="stylesheet"),
    html.Link(href="https://fonts.googleapis.com/css?family=Open+Sans", rel="stylesheet"),
    html.Link(href="https://fonts.googleapis.com/css?family=Ubuntu", rel="stylesheet"),
    html.Link(href="https://cdn.rawgit.com/amadoukane96/8a8cfdac5d2cecad866952c52a70a50e/raw/cd5a9bf0b30856f4fc7e3812162c74bfc0ebe011/dash_crm.css", rel="stylesheet")

    ],
    className="row",
    style={"margin": "0%"},
)

@app.callback(Output("tab_content", "children"), [Input("tabs", "value")])
def render_content(tab):
    if tab == "network":
        return network.layout
    elif tab == "topics":
        return topics.layout
    else:
        return home.layout



if __name__ == '__main__':
    app.run_server(debug=True)
