import os
import pickle as pkl
import numpy as np
import pandas as pd
from Topic import Topic
import dash_html_components as html
from WikiLoader import WikiLoader
from plot_helpers import make_branch_to_person_graph, get_formatted_preds,format_year, get_people_heights


def load_topic(title):
    pth = os.path.join(os.getcwd(), 'topics', title+'.pkl')
    obj = None

    if os.path.exists(pth):
        file = open(pth, 'rb')  
        obj = pkl.load(file)
        file.close()
        
    else: 
        file = open(pth, 'wb')
        loader = WikiLoader(dr='WikiPages/', save_mode=True, multi=multiprocessing.cpu_count())
        obj = Topic(loader, topic)
        pkl.dump(obj, file)
        file.close()
    return obj

        
# return html Table with dataframe values  
def df_to_table(df):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in df.columns])] +
        
        # Body
        [
            html.Tr(
                [
                    html.Td(df.iloc[i][col])
                    for col in df.columns
                ]
            )
            for i in range(len(df))
        ]
    )


def get_people_df(topic):
    branches = topic.branches.keys()
    people = topic.get_all_people()

    #Get each of the following values in the col
    cols = ['name', 'all_years', 'born', 'dead']
    df = pd.DataFrame([[getattr(i,j) for j in cols] for i in people.values() if len(getattr(i, 'all_years')) > 0], columns=cols)

    #Get persons mean year
    df['year_mean'] = df.all_years.apply(np.mean)

    #Create a formatted list of branches a person belongs to
    G = make_branch_to_person_graph(topic.branches)
    people_dict = get_formatted_preds(G)
    df['desc'] = [people_dict[n] if n in people_dict else topic.name for n in df.name]

    #Custom format the heights each person dot should go
    df['y_vals'] = get_people_heights(df.year_mean, 50)

    return df

